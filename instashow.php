<?php

/*
Plugin Name: Instashow
Description: Allows to fetch and display latest posts from Instagram page without using API
Version: 1.0
Requires PHP: 7.4
Author: Adrian
License: GPL2
*/

declare(strict_types=1);

namespace Instashow;

define('DIR_PATH', plugin_dir_path( __FILE__ ));

require DIR_PATH . 'vendor/autoload.php';

use Instashow\Service\{Template, InstagramWrapper};

class Instashow
{
    public function __construct()
    {
        add_shortcode('instashow', [$this, 'shortcode_handler']);
    }

    /**
     * @param array $atts Parameters passed from the shortcode call e.g. [instashow page="spacex"] gives ['page' => 'spacex']
     * @return string rendered HTML
     */
    public function shortcode_handler(array $atts): string
    {
        $instagram = new InstagramWrapper($atts['page']); // Create InstagramWrapper with page name passed in shortcode call e.g. [instashow page="spacex"]
        $view = new Template('unstyled', ['urls' => $instagram->get_images_urls()]);

        return $view->render();
    }
}

new Instashow();