<?php

declare(strict_types=1);

namespace Instashow\Service;

use RuntimeException;

class InstagramWrapper
{
    private string $page_name;

    private string $endpoint;

    /**
     * InstagramWrapper constructor.
     * @param string $page_name A valid Instagram page name e.g. 'spacex'
     */
    public function __construct(string $page_name)
    {
        $this->page_name = $page_name;
        $this->endpoint = "https://www.instagram.com/{$page_name}/?__a=1";
    }

    /**
     * Fetches JSON data from the endpoint
     * @return array If request succeeded returns PHP associative array with response from the endpoint. If request failed and WordPress debugging is off returns empty array.
     * @throws RuntimeException If request failed and WordPress debugging is on.
     */
    private function get_endpoint_response(): array
    {
        $response = file_get_contents($this->endpoint);

        if(!$response) // if the fetch failed
        {
            if(WP_DEBUG)
            {
                throw new RuntimeException('Failed to fetch Instagram JSON');
            }
            else
            {
                return []; // we don't want this plugin to crash the website for users if it can't load
            }
        }

        return json_decode($response, true);
    }

    /**
     * Searches for Instagram page's latest images URLs in endpoint's response
     * @return array If found - Instagram page's latest images urls. If not found any - empty array.
     */
    public function get_images_urls(): array
    {
        $endpoint_response = $this->get_endpoint_response();

        if([] !== $endpoint_response)
        {
            $images = $endpoint_response['graphql']['user']['edge_owner_to_timeline_media']['edges'];

            if($images)
            {
                return array_map(fn($image) => $image['node']['display_url'], $images);
            }
        }

        return [];
    }
}