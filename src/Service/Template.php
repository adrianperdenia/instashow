<?php

declare(strict_types=1);

namespace Instashow\Service;

use {RuntimeException, UnexpectedValueException};

class Template
{
    private string $template_path;

    private array $variables = [];

    /**
     * Template constructor.
     * @param string $template name of the template file you want to use, e.g. 'unstyled'
     * @param array $variables e.g. ["name" => "John", "age" => 25] will make $name and $age variables available to use in a template
     */
    public function __construct(string $template, array $variables)
    {
        $this->template_path = DIR_PATH . "/templates/{$template}.php";
        $this->assign_variables($variables);
    }

    /**
     * Validates and sets variables which will be available in rendered template
     * @param array $variables
     * @throws UnexpectedValueException if any of given variables name is not a valid PHP variable name
     */
    private function assign_variables(array $variables): void
    {
        foreach($variables as $variable_name => $variable_value)
        {
            if(!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $variable_name))
            {
                throw new UnexpectedValueException('Variable name "' . $variable_name . '" is invalid');
            }
        }

        $this->variables = $variables;
    }

    /**
     * Renders the template
     * @return string rendered HTML
     * @throws RuntimeException when template file not found
     */
    public function render(): string
    {
        if(!is_file($this->template_path))
        {
            throw new RuntimeException('Template "' . $this->template_path . '" not found');
        }

        extract($this->variables);
        ob_start();
        require($this->template_path);
        return ob_get_clean();
    }
}